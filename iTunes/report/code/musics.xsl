<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" 
version="1.0"
encoding="iso-8859-1" 
indent="yes" />
<xsl:param name="owner"/>
<xsl:param name="firstIDmusic"/>

<xsl:template match="/">
<xsl:element name="Musics">
<xsl:apply-templates select="plist/dict/dict/dict"/>
</xsl:element>
</xsl:template>

<xsl:template match="plist/dict/dict/dict">
<xsl:variable name="count">
<xsl:number/>
</xsl:variable>
<xsl:if test="key[.='Kind']/following-sibling::node()[1]='MPEG audio file'">
<xsl:element name="Music">
<xsl:attribute name="musicID">
<xsl:value-of select="concat('music',$count+$firstIDmusic)"/>
</xsl:attribute>
<xsl:attribute name="owner">
<xsl:value-of select="$owner"/>
</xsl:attribute>
<xsl:element name="TrackID">
<xsl:value-of select="key[.='Track ID']/following-sibling::node()[1]"/>
</xsl:element>
<xsl:element name="Title">
<xsl:value-of select="key[.='Name']/following-sibling::node()[1]"/>
</xsl:element>
<xsl:call-template name="author" />
<xsl:call-template name="time" />
<xsl:call-template name="genre" />
<xsl:call-template name="bitRate" />
<xsl:call-template name="release" />
<xsl:element name="Coments"></xsl:element>
<xsl:element name="Shares"></xsl:element>
</xsl:element>
</xsl:if>
</xsl:template>

<xsl:template name="author">
<xsl:element name="Author">
<xsl:value-of select="key[.='Artist']/following-sibling::node()[1]"/>
</xsl:element>
</xsl:template>

<xsl:template name="genre">
<xsl:element name="Genre">
<xsl:value-of select="key[.='Genre']/following-sibling::node()[1]"/>
</xsl:element>
</xsl:template>

<xsl:template name="time">
<xsl:element name="Time">
<xsl:value-of select="key[.='Total Time']/following-sibling::node()[1]"/>
</xsl:element>
</xsl:template>

<xsl:template name="bitRate">
<xsl:element name="BitRate">
<xsl:value-of select="key[.='Bit Rate']/following-sibling::node()[1]"/>
</xsl:element>
</xsl:template>

<xsl:template name="release">
<xsl:element name="ReleaseDate">
<xsl:value-of select="key[.='Release Date']/following-sibling::node()[1]"/>
</xsl:element>
</xsl:template>

</xsl:stylesheet>
