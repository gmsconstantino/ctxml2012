<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" 
version="1.0"
encoding="iso-8859-1" 
indent="yes" />
<xsl:param name="kind"/>
<xsl:param name="owner"/>
<xsl:param name="firstIDplaylist"/>

<xsl:key name="chave" match="/plist/dict/dict/dict" use="key[.='Album']/following-sibling::node()[1]"/>

<xsl:template match="/">
<xsl:element name="Playlists">
<xsl:apply-templates select="plist/dict/dict"/>
</xsl:element>
</xsl:template>

<xsl:template match="plist/dict/dict">
<xsl:for-each select="dict[generate-id(.)=generate-id(key('chave',string)[1]) and string='MPEG audio file']">
<xsl:variable name="count">
<xsl:number/>
</xsl:variable>
<xsl:element name="Playlist">
<xsl:attribute name="playlistID">
<xsl:value-of select="concat('play',$count+$firstIDplaylist)"/>
</xsl:attribute>
<xsl:attribute name="owner">
<xsl:value-of select="$owner"/>
</xsl:attribute>
<xsl:attribute name="kind">
<xsl:value-of select="$kind"/>
</xsl:attribute>
<xsl:element name="Name">
<xsl:value-of select="key[.='Album']/following-sibling::node()[1]"/>
</xsl:element>
<xsl:call-template name="purpose"/>
<xsl:element name="Musics">
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:template>

<xsl:template name="purpose">
<xsl:element name="Purpose">
<xsl:value-of select="key[.='Artist']/following-sibling::node()[1]"/>
</xsl:element>
</xsl:template>

<xsl:template name="musics">
<xsl:for-each select="key[.='Track ID']/following-sibling::node()[1]">
<xsl:element name="MusicID">
<xsl:apply-templates select="."/>
</xsl:element>
</xsl:for-each>
</xsl:template>

<xsl:template name="track" match="/plist/dict/array/dict/array/dict">
<xsl:value-of select="integer/text()"/>
</xsl:template>

</xsl:stylesheet>
