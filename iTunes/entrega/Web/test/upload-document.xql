xquery version "1.0";


let $collection := '/db/itunes'
let $filename := request:get-uploaded-file-name('file')
 
(: make sure you use the right user permissions that has write access to this collection :)
let $login := xmldb:login($collection, 'admin', '1234')
let $store := xmldb:store($collection, $filename, request:get-uploaded-file-data('file'))
 
return
<results>
   <message>File {$filename} has been stored at collection={$collection}.</message>
</results>
