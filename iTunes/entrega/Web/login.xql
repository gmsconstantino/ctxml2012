xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

declare variable $database-uri as xs:string { "xmldb:exist:///db" };
declare variable $redirect-uri as xs:anyURI { xs:anyURI("user.xql") };

declare function local:login($user as xs:string) as element()?
{
    let $pass := request:get-parameter("pass", ""),
        $login := xdb:authenticate($database-uri, $user, $pass)
    return
        if ($login) then (
            session:set-attribute("user", $user),
            session:set-attribute("password", $pass),
            response:redirect-to(session:encode-url($redirect-uri))
        ) else
            <p>Login failed! Please retry.</p>
};

declare function local:do-login() as element()?
{
    let $userName := request:get-parameter("user", ()),
		$user := doc("/db/itunes/users.xml")/Users/User[Attributes/NomeUtilizador = $userName]
    return
        if ($user) then
            local:login(data($user/@userID))
        else ()
};

session:invalidate(),
session:create(),
let $body :=
  <div id="best">
  <form name="login" action="{session:encode-url(request:get-uri())}"><!-- onsubmit="return checkForm()"-->
  <table align="center">
    <tr>  
      <td> 
        Nome de Utilizador: 
      </td>
      <td>
        <input type="text" name="user"/>
      </td>
    </tr>
    <tr>
      <td>
        Password:
      </td>
      <td>
        <input type="password" name="pass"/>
      </td>
    </tr>
    <tr>
      <td style="font-size: 8pt">
        <a href="recover.xql">Recuperar a password</a>
      </td>
      <td>
        <input type="reset" value="Limpar"/>
        <input type="submit" value="Login"/>
      </td>
    </tr>
    <tr>
      <td id="loginWarning">
        {local:do-login()}
      </td>
    </tr>
  </table>
  </form>
  </div>

let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:main_head(())}
{skel:main_body($body)}
</html>

return $html
