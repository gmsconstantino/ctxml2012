xquery version "1.0";

declare namespace exist = "http://exist.sourceforge.net/NS/exist";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace transform="http://exist-db.org/xquery/transform";

declare option exist:serialize "method=xml media-type=application/xml 
process-xsl-pi=no omit-xml-declaration=no";

declare function local:replaceMusicIDs($elements as element()*,$values as element()*) as element()*
{
  for $idT in $elements/Playlists/Playlist/Musics/MusicID
	return 
		(
		let $id := string($values/Musics/Music[TrackID=$idT]/@musicID)
		let $ret :=  update replace $idT with $id
		return ()
		)
};

let $collection := 'xmldb:exist:///db/itunes'
let $login := xmldb:login($collection, 'admin', '1234')

let $owner := 'user2', 
	$xml := doc("/db/itunes/iTunes_mini.xml"),
	$xslt := doc("/db/itunes/stylesheets/playlists.xsl"),
	$params := 
		<parameters>
			<param name="owner"  value="{$owner}"/>
			{
				for $p in request:parameter-names()
				let $val := request:get-parameter($p,())
					where  not($p = ("xml","xslt"))
				return
					<param name="{$p}"  value="{$val}"/>
		    }
		</parameters>
 
let $playlists := transform:transform($xml, $xslt, $params)
(: Inserir resultado na BD :)
let $docP := doc('/db/itunes/playlistsT.xml')
let $updateP := update insert $playlists/Playlist into $docP/Playlists

let $musicas := doc('/db/itunes/user2/musics.xml')

(: Actualizar Playlist com id's correctas :)
(: let $temp := local:replaceMusicIDs($docP,$musicas) :)

for $id in $docP/Playlists/Playlist/Musics/MusicID
	return 
		(
			let $teste := update replace $id with <MusicID>{string($musicas/Musics/Music[TrackID=$id]/@musicID)}</MusicID>
			return ()
		)
