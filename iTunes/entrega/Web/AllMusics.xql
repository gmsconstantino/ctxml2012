xquery version "1.0"; 

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare namespace session="http://exist-db.org/xquery/session";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

let $user-id := session:get-attribute("user")
let $body :=
  <div id="content">
    <div>
      <table>
          <tr>
            <th>Title</th>
            <th>Time</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>BeatRate</th>
            <th>BitRate</th>
            <th>ReleaseDate</th>
          </tr>
        {corba:fetchAllMusics()}
      </table>
    </div>
    <div style="text-align:right;">
      {corba:pagination(count(collection('/db/itunes')/Musics/Music) div 20,"AllMusics.xql")}
    </div>
  </div>

let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html
