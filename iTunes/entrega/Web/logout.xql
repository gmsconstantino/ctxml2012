xquery version "1.0";

import module namespace session="http://exist-db.org/xquery/session";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";


session:invalidate(),
let $body :=
  <div>
    <h2>GoodBye</h2>
    <p>You have logged out of the application.</p>
    <p>
      <a href="/exist/itunes">Back to Home</a>
    </p>
  </div>


let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:main_head(())}
{skel:main_body($body)}
</html>

return
  $html



