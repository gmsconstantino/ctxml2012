xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";
import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

let $check := request:get-parameter('user',())
let $user := 
	if ($check) then
		corba:get-user($check)
	else corba:main()

let $body :=
<div id="content">
{
  if($check) then ()
  else <h1>Welcome! {corba:username()}.</h1>
}
  <br/>
  <table align="center">
		<tr>
			<td> <img src="https://secure.gravatar.com/avatar/49d85eca1cebd6245348f1acf8d8d30b?d=https%3A%2F%2Fdwz7u9t8u8usb.cloudfront.net%2Fm%2F2219cd120e6a%2Fimg%2Fdefault_avatar%2F32%2Fuser_blue.png&amp;s=32"  alt="Foto" height="50" width="50"/> </td>
			<td> Registado desde <br/>{$user/Attributes/DataRegisto}</td>
		</tr>
		<tr>
			<td> Nick: </td>
			<td> {$user/Attributes/NomeUtilizador}</td>
		</tr>
		<tr>
			<td> Nome Completo: </td>
			<td> {$user/Attributes/NomeCompleto}</td>
		</tr>
		<tr>
			<td> Data Nascimento: </td>
			<td> {$user/Attributes/DataNascimento}</td>
		</tr>
		<tr>
			<td> Email: </td>
			<td> {$user/Attributes/Email}</td>
		</tr>
		<tr>
{
   if($check) then ()
   else
			<a href="userDetails.xql"><span>Actualizar</span></a>
}
		</tr>
	  </table>
</div>


let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html

