var urlParams = {};

$(function(){

  $('#bt_register').click(function(){
    window.location.href = "register.xql";
  });
  
  $('#bt_login').click(function(){
    window.location.href = "login.xql";
  });
  
  $('#bt_upload').click(function(){
    window.location.href = "uploadForm.xql";
  });
  
  $('#bt_logout').click(function(){
    window.location.href = "logout.xql";
  });

  $('#linkAddComment').click(function(){
    $('#div_form').show();
  });

  $('#linkUpdateMusic').click(function(){
    urlQuery();
    window.location.href = "updateMusicForm.xql?musicID="+urlParams["musicID"]+"&user="+urlParams["user"];
  });

  $('#formCom_cancel').click(function(){
    $('#div_form').hide();
    return false;
  });

  $('#formMusic_cancel').click(function(){
    urlQuery();
    var url = "music.xql?musicID="+urlParams["musicID"]
    url += "&user="+urlParams["user"]
    window.location.href = url;
    return false;
  });

  $('#bt_search').click(function () {
    if($("#searchtext").val().trim() != "")
      window.location.href = "search.xql?query="+$("#searchtext").val().trim()
  })

  //Set click on icon search and on Enter
  //
  $('#searchtext').keypress(function(ev){
    if(ev.which == 13){
      window.location.href = "search.xql?query="+$("#searchtext").val().trim()
      return false;
    }
  });

  $('#formFriend_cancel').click(function(){
    $("#searchF").val() = "";
    return false;
  });

}
);


function urlQuery() {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams['musicID']='';
    urlParams['user']='';

    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
};





