xquery version "1.0";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";
import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare namespace exist = "http://exist.sourceforge.net/NS/exist";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace transform="http://exist-db.org/xquery/transform";

declare option exist:serialize "method=xml media-type=application/xml 
process-xsl-pi=no omit-xml-declaration=no";

declare function local:setParams($owner as xs:string, $kind as xs:string, $firstIDmusic as xs:string, $firstIDplaylist as xs:string) as element()
{
<parameters>
  <param name="owner"  value="{$owner}"/>
  <param name="kind"  value="{$kind}"/>
  <param name="firstIDmusic" value="{$firstIDmusic}"/>
  <param name="firstIDplaylist" value="{$firstIDplaylist}"/>
  {
    for $p in request:parameter-names()
    let $val := request:get-parameter($p,())
    where  not($p = ("xml","xslt"))
    return
      <param name="{$p}"  value="{$val}"/>
    }
</parameters>
};

declare function local:replaceMusicIDs($kind as xs:string) as element()*
{	
let $owner := session:get-attribute("user")
let $docM := doc(concat('/db/itunes/',$owner,'/musics.xml'))
let $docP := doc(concat('/db/itunes/',$owner,'/playlists.xml'))
for $id in $docP/Playlists/Playlist[@kind=$kind]/Musics/MusicID
	return update replace $id with <MusicID>{string($docM/Musics/Music[TrackID=$id]/@musicID[1])}</MusicID>
};

declare function local:updateUserPlaylists($novos as element()) as element()*
{	
let $owner := session:get-attribute("user")
let $raiz := doc('/db/itunes/users.xml')/Users/User[@userID=$owner]
for $each in $novos/Playlist
	return update insert <PlaylistID>{string($each/@playlistID)}</PlaylistID> into $raiz/Playlists

};

    
let $owner := session:get-attribute("user"),
    $pass := session:get-attribute("password")


let $collection := concat('xmldb:exist:///db/itunes/',$owner)
let $login := xmldb:login($collection, $owner, $pass)
let $filename := "lib.xml"
let $store := xmldb:store($collection, $filename, request:get-uploaded-file-data('file'))

let $docM := doc(concat('/db/itunes/',$owner,'/musics.xml'))
let $docP := doc(concat('/db/itunes/',$owner,'/playlists.xml'))

let	$xsltM := doc("/db/itunes/stylesheets/musics.xsl"),
	$xsltP := doc("/db/itunes/stylesheets/playlists.xsl"),
  $xsltA := doc("/db/itunes/stylesheets/albums.xsl"),
	$file := doc(concat("/db/itunes/",$owner,"/",$filename))

let $kind := 'custom',
	$mxM := max(for $each in $docM/Musics/Music/@musicID return number(substring-after(string($each), 'music'))),
	$firstIDmusic := if ($mxM) then 
				string($docM/Musics/Music[@musicID = concat('music',$mxM)]/@musicID)
			else string("0"),
	$mxP := max(for $each in $docP/Playlists/Playlist/@playlistID return number(substring-after(string($each), 'play'))),
	$firstIDplaylist := if ($mxP) then 
				string($docP/Playlists/Playlist[@playlistID = concat('play',$mxP)]/@playlistID)
			else string("0"),
	$params := local:setParams($owner,$kind,$firstIDmusic,$firstIDplaylist)

(: Criar Musicas :)		
let $musicas := transform:transform($file, $xsltM, $params)
let $updateM := update insert $musicas/Music into $docM/Musics

(: Criar/Transformar playlists :)
let $playlists := transform:transform($file, $xsltP, $params)
let $updateP := update insert $playlists/Playlist into $docP/Playlists

(: Criar/Transformar playlists Albums:)
let $mxM2 := max(for $each in $docM/Musics/Music/@musicID return number(substring-after(string($each), 'music')))
let $firstIDmusic2 := if ($mxM) then 
      string($docM/Musics/Music[@musicID = concat('music',$mxM2)]/@musicID)
    else string("0"),
$mxP2 := max(for $each in $docP/Playlists/Playlist/@playlistID return number(substring-after(string($each), 'play'))),
$firstIDplaylist2 := if ($mxP2) then 
      string($docP/Playlists/Playlist[@playlistID = concat('play',$mxP2)]/@playlistID)
    else string("0")
let $albums := transform:transform($file, $xsltA, local:setParams($owner,"Album",$firstIDmusic2,$firstIDplaylist2))
let $updateP := update insert $albums/Playlist into $docP/Playlists

(: Inserir Musicas nos albums, vai ser pedido por ajax o tratamento :)
(:let $p := $docP/Playlists:)
(:for $dict in $file/plist/dict/dict/dict:)
(:let $music-id := $dict/integer[1]:)
(:let $music-name := $dict/key[.='Album']/following-sibling::node()[1]:)
(:let $coiso := update insert <MusicID>{string($music-id)}</MusicID> into $p/Playlist[@kind="Album" and Name=$music-name]/Musics:)

(:[> Actualizar as playlist do user com os albums vazios<]:)
(:let $updateA := local:updateUserPlaylists($albums):)

(: 
	Percorrer playlists.xml e a cada MusicID 
  procura a respectiva ID nas musicas e substitui
:)
let $replace := local:replaceMusicIDs("custom")

(: Actualizar as playlists deste user :)
let $update := local:updateUserPlaylists($playlists)

(:Resultado:)
let $body :=
  <div>{
    if ($playlists)
    then (
      <span>Upload Completo</span>
    ) else (
      <span>Upload Falhado</span>
    )
  }</div>

let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html



