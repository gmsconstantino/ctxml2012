xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";
import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

declare variable $redirect-uri as xs:anyURI { xs:anyURI("userDetails.xql") };

declare function local:do-update() as element()?
{
	let $collection := 'xmldb:exist:///db/itunes/'
	let $login := xmldb:login($collection, 'admin', 'asd^159')
	
    let $userNick := request:get-parameter("nick", ()),
		$userName := request:get-parameter("nome", ()),
		$dn := request:get-parameter("data", ()),
		$email := request:get-parameter("email", ()),
		$change := request:get-parameter("change",()),
		$currentUser := session:get-attribute("user"),
		$user := doc("/db/itunes/users.xml")/Users/User[@userID = $currentUser]
		return
			if ($change='true') then 
					(
					if (not($userNick = $user/Attributes/NomeUtilizador)) then
						update value $user/Attributes/NomeUtilizador with string($userNick)
					else
						(),
					if (not($userName = $user/Attributes/NomeCompleto)) then
						update value $user/Attributes/NomeCompleto with string($userName)
					else
						(),
					if (not($dn = $user/Attributes/DataNascimento)) then
						update value $user/Attributes/DataNascimento with string($dn)
					else
						(),
					if (not($email = $user/Attributes/Email)) then
						update value $user/Attributes/Email with string($email)
					else
						()
					)
			else
				()
				
};

let $user := corba:main()

let $body :=
<div id="content">
  <h2> Pagina de Edicao {local:do-update()} </h2>
  <form name="edit" action="{session:encode-url(request:get-uri())}">
    <input type="hidden" name="change" value="true" />
	  <table align="center">
		<tr>
			<td> <img src="https://secure.gravatar.com/avatar/49d85eca1cebd6245348f1acf8d8d30b?d=https%3A%2F%2Fdwz7u9t8u8usb.cloudfront.net%2Fm%2F2219cd120e6a%2Fimg%2Fdefault_avatar%2F32%2Fuser_blue.png&amp;s=32"  alt="Foto" height="50" width="50"/> </td>
			<td> Registado desde <br/>{$user/Attributes/DataRegisto}</td>
		</tr>
		<tr>
			<td> Nick: </td>
			<td> <input type="text" name="nick" value="{$user/Attributes/NomeUtilizador}"/></td>
		</tr>
		<tr>
			<td> Nome Completo: </td>
			<td> <input type="text" name="nome" value="{$user/Attributes/NomeCompleto}"/></td>
		</tr>
		<tr>
			<td> Data Nascimento: </td>
			<td> <input type="text" name="data" value="{$user/Attributes/DataNascimento}"/></td>
		</tr>
		<tr>
			<td> Email: </td>
			<td> <input type="text" name="email" value="{$user/Attributes/Email}"/></td>
		</tr>
		<tr>
			<input type="submit" value="Actualizar"/>
		</tr>
	  </table>
  </form>
</div>


let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html

