xquery version "1.0"; 

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

declare function local:search($query as xs:string, $user-id as xs:string) as element()*
{
  let $rstmusic := doc(concat('/db/itunes/',$user-id,"/musics.xml"))/Musics/Music[ft:query(.,$query)]
  return
    if($rstmusic)
    then
      for $m in $rstmusic
        return
          <li>
            <input type="checkbox" name="music" value="{$m/@musicID}">{data($m/Title)},  {data($m/Author)}</input>
          </li>
   else ()
};

declare function local:addMusics($user-id as xs:string, $playlist as xs:string, $musics as xs:string*) as element()*
{
  let $owner := session:get-attribute("user"),
      $pass := session:get-attribute("password")
  let $collection := concat('xmldb:exist:///db/itunes/',$owner)
  let $login := xmldb:login($collection, $owner, $pass)

  let $doc := doc(concat("/db/itunes/",$user-id,"/playlists.xml"))/Playlists/Playlist[@playlistID = $playlist]
  for $m in $musics
  let $add := update insert <MusicID>{$m}</MusicID> into $doc/Musics
  return
      <p>Add music {$m} to the playlist.</p>
};

declare function local:random($max as xs:integer) as xs:integer
{
    let $r := ceiling(util:random() * $max) cast as xs:integer
    return $r
};

declare function local:do-update() as element()?
{
  let $owner := session:get-attribute("user"),
      $pass := session:get-attribute("password"),
      $collection := 'xmldb:exist:///db/itunes',
      $login := xmldb:login($collection, "admin", "1234")

  let $playlistPr := request:get-parameter("playlist","")
  let $db := doc(concat('/db/itunes/',$owner,'/playlists.xml'))
  let $doc := $db/Playlists/Playlist[@playlistID=$playlistPr],
      $namePr := request:get-parameter("name",())
  return 
    let $aa := if($playlistPr="none") then 
      let $a := if($namePr) then
        update value $doc/Name with string($namePr)
      else ()
      let $b := if(request:get-parameter("description",())) then
        update value $doc/Purpose with string(request:get-parameter("description",()))
      else ()
      return ()
    else
          let $idp := concat('play',local:random(1000000))
          let $p := <Playlist playlistID="{$idp}" owner="{$owner}" kind="custom">
                    <Name>{string($namePr)}</Name><Purpose>{request:get-parameter("description",())}</Purpose><Musics/></Playlist>
          let $i := update insert $p into $db/Playlists
          let $xx := update insert <playlistID>{$idp}</playlistID> into doc('/db/itunes/users.xml')/Users/User[@userID=$owner]/Playlists
          return ()
    return ()
};

let $update := local:do-update()
let $user-id := session:get-attribute("user")
let $playlistPr := request:get-parameter("playlist",())
let $searchPr := request:get-parameter("search",())
let $musicsPr := request:get-parameter("music",())

let $doc := doc(concat('/db/itunes/',$user-id,'/playlists.xml'))/Playlists/Playlist[@playlistID=$playlistPr]
let $body :=
<div id="content">
  <h1>Create / Update Playlist</h1>
  <form method="get" action="playlistForm.xql">
    <fieldset>
      <legend>Attributes:</legend>
      <label>Name</label>
      <div>
        <input name="name" value="{data($doc/Name)}" type="text"/> 
      </div> 
      <label>Description</label>
      <div>
        <textarea name="description" cols="40" rows="4" value="">{data($doc/Purpose)}</textarea> 
      </div> 
      <input type="hidden" name="playlist" value="{if($playlistPr) then $playlistPr else string('none')}"/>
      <input type="submit" value="Submit"/>
    </fieldset>
	</form>
{
  if(not($playlistPr="none")) then
      <fieldset>
        <legend>Add Musics:</legend>
        <form method="get" action="playlistForm.xql">
          <label>Search</label>
          <div>
            <input name="search" type="text"/> 
            <input type="submit" value="Search"/>
          </div> 
          <input type="hidden" name="playlist" value="{$playlistPr}"/>
        </form>
        {
        if($searchPr) then
          let $search := local:search($searchPr,$user-id)
          let $r1 :=
            if(count($search)>0) then
              <form action="playlistForm.xql" method="get">
                <ul style="list-decoration:none;">
                  {$search}
                </ul>
                <input type="hidden" name="playlist" value="{$playlistPr}"/>
                <input type="submit" value="Add Musics"/>
              </form>
            else <span>No results. Your musics only.</span>
          return $r1
        else ()
        }
        <div>{
        if($musicsPr) then
          let $r2 := local:addMusics($user-id,$playlistPr,$musicsPr)
          for $r3 in $r2
          return $r3
        else ()
        }</div>
      </fieldset>
  else ()
}
</div>

let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html

