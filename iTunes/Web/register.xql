xquery version "1.0";
import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

let $s :=
  <script type="text/javascript" src="js/register.js" language="javascript">
  </script>

let $body :=
<div>
  <form id='fm_register' method="post" action="new_user.xql">
    <fieldset>
      <legend>Register</legend>

      <table>
        <tr>
          <td>Fist name</td>
          <td>
            <input name="firstname" value="" type="text"/>
          </td>
        </tr>
        <tr>
          <td>Last name</td>
          <td>
            <input name="lastname" value="" type="text"/>
          </td>
        </tr>
        <tr>
          <td>Date Of Birth</td>
          <td>
            <select name="month" onChange="changeDate(this.options[selectedIndex].value);">
              <option value="na">Month</option>
              <option value="1">January</option>
              <option value="2">February</option>
              <option value="3">March</option>
              <option value="4">April</option>
              <option value="5">May</option>
              <option value="6">June</option>
              <option value="7">July</option>
              <option value="8">August</option>
              <option value="9">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
            <select name="day" id="day">
              <option value="na">Day</option>
            </select>
            <select name="year" id="year">
              <option value="na">Year</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>Username</td>
          <td>
            <input id="sign_up_user" name="username" value="" type="text"/>
          </td>
        </tr>
        <tr>
          <td>Email</td>
          <td>
            <input id="sign_up_email" name="email" value="" type="text"/>
          </td>
        </tr>
        <tr>
          <td>Password</td>
          <td>
            <input id="sign_up_password" name="password" value="" type="password"/>
          </td>
        </tr>
        <tr>
          <td>Password (again)</td>
          <td>
            <input id="sign_up_cpassword" name="cpassword" value="" type="password"/>
          </td>
        </tr>
        <tr>
          <td>
            <input type="submit" value="Sign Up"/>
            <a class="cancel" href="index.html">Cancel</a>
          </td>
          <td></td>
        </tr>
      </table>
    </fieldset>
  </form>
</div>


let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:main_head($s)}
{skel:main_body($body)}
</html>

return
  $html
