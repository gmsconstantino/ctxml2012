xquery version "1.0";

<html>
<head><title>Test Page</title></head>
<body>
<h1>My Library</h1>
<ul>
{
for $x in doc("/db/itunes/books.xml")/bookstore/book
(:order by $x:)
return <li>{data($x/title)}&#032;{data($x/price)}$</li>
}
</ul>
<hr/>
<form action="update_book.xql" method="get">
ID: <input type="text" name="id"/>
Price: <input type="text" name="price"/>
<input type="submit" value="Submit"/>
</form>
</body>
</html>
