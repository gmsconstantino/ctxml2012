xquery version "1.0";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace xs="http://www.w3.org/2001/XMLSchema";

let $id:= request:get-parameter("id",0)
let $price:= request:get-parameter("price",0)

let $doc := doc('/db/itunes/books.xml')
let $bla := update value $doc/bookstore/book[xs:integer($id)]/price with $price

return
<html>
<head><title>Test Page</title></head>
<body>
<h2>Record updated successfully</h2>
<p></p>
<a href='test.xql'>Back</a>
</body>
</html>
