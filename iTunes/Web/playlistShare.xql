xquery version "1.0"; 

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare namespace session="http://exist-db.org/xquery/session";
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

declare function local:getPlaylists() as element()*
{
  let $user-id := session:get-attribute("user")
  let $guess := doc(concat("/db/itunes/",$user-id,"/playlists.xml"))
  for $playlist in $guess/Playlists/Playlist
  return
    <option value="{data($playlist/@playlistID)}">{data($playlist/Name)}</option>
};

declare function local:getFriends() as element()*
{
  let $user-id := session:get-attribute("user")
  let $guess := doc("/db/itunes/users.xml")
  for $fw-user in $guess/Users/User[@userID=$user-id]/Following/User
  let $fw-name := $guess/Users/User[@userID=string($fw-user)]/Attributes/NomeCompleto
  return
    <option value="{data($fw-user)}">{data($fw-name)}</option>
};

let $user-id := session:get-attribute("user")
let $playlist-sh := request:get-parameter("playlist",())
let $user-sh := request:get-parameter("touser",())
let $body :=
<div id="content">
<h1>Share Playlist</h1>
  <form method="get" action="playlistShare.xql">
      <fieldset>
        <div>
          <select name="playlist">
            <option value="na">Playlist</option>
            {local:getPlaylists()}
          </select>
          <select name="touser">
            <option value="na">Friend</option>
            {local:getFriends()}
          </select>
        </div>
        <input type="reset" value="Reset"/>
        <input type="submit" value="Submit"/>
      </fieldset>
		</form>
{
  if($playlist-sh and $user-sh) then
    let $doc := doc('/db/itunes/shares.xml')
    let $test := $doc/Shares/Share[@owner=$user-id and PlaylistID=$playlist-sh]
    let $a := 
      if(count($test)=0) then
        let $share := 
          <Share shareID="share{count(doc('/db/itunes/shares.xml')/Shares/Share)}" owner="{$user-id}"><PlaylistID>{$playlist-sh}</PlaylistID><UserID>{$user-sh}</UserID></Share>
        let $b := update insert $share into $doc/Shares
        return
          <p>You shared successfully a playlist.</p>
      else if(count($test)>0) then
        let $shuser := <UserID>{$user-sh}</UserID>
        let $b := update insert $shuser into $test
        return
          <p>You shared successfully the playlist.</p>
      else ()
    return $a
  else ()
}
</div>

let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html

