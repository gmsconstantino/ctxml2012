<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" 
        version="1.0"
        encoding="iso-8859-1" 
        indent="yes" />
    <xsl:param name="kind"/>
    <xsl:param name="owner"/>
    <xsl:param name="firstIDplaylist"/>
    
    <xsl:template match="/">
        <xsl:element name="Playlists">
            <xsl:apply-templates select="plist/dict/key[.='Playlists']/following-sibling::node()"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="plist/dict/array/dict">
        <xsl:variable name="count">
            <xsl:number/>
        </xsl:variable>
        <xsl:if test="self::node()[not(key='Distinguished Kind') and not(key='Master') and not(key='Folder')]">
			<xsl:element name="Playlist">
				<xsl:attribute name="playlistID">
					<xsl:value-of select="concat('play',$count+$firstIDplaylist)"/>
				</xsl:attribute>
				<xsl:attribute name="owner">
					  <xsl:value-of select="$owner"/>
				  </xsl:attribute>
				  <xsl:attribute name="kind">
					  <xsl:value-of select="$kind"/>
				  </xsl:attribute>
				<xsl:element name="Name">
					<xsl:value-of select="key[.='Name']/following-sibling::node()[1]"/>
				</xsl:element>
				<xsl:call-template name="purpose"/>
				<xsl:element name="Musics">
					<xsl:call-template name="musics"/>
				</xsl:element>
			</xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="purpose">
        <xsl:element name="Purpose"/>
    </xsl:template>
    
    <xsl:template name="musics">
        <xsl:for-each select="key[.='Playlist Items']/following-sibling::array/dict">
         <xsl:element name="MusicID">
             <xsl:apply-templates select="."/>
         </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="track" match="/plist/dict/array/dict/array/dict">
        <xsl:value-of select="integer/text()"/>
    </xsl:template>
    
</xsl:stylesheet>
