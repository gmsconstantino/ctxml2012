<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <xsl:element name="Musics">
      <xsl:apply-templates select="plist/dict/dict/key"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="key">
    <xsl:if test="following-sibling::node()/key[6]/following-sibling::node()='MPEG audio file'">
      <xsl:element name="Music">
        <xsl:attribute name="musicID">
          <xsl:value-of select="text()"/>
        </xsl:attribute>
        <xsl:element name="Teste">
          <xsl:value-of select="following-sibling::node()/key[6]/following-sibling::node()"/>
        </xsl:element>
        <xsl:element name="Title">
          <xsl:value-of select="following-sibling::node()/key[2]/following-sibling::node()/text()"/>
        </xsl:element>
        <xsl:element name="Author">
          <xsl:value-of select="following-sibling::node()/key[3]/following-sibling::node()/text()"/>
        </xsl:element>
        <xsl:element name="Genre">
          <xsl:value-of select="following-sibling::node()/key[7]/following-sibling::node()/text()"/>
        </xsl:element>
        <xsl:element name="BitRate">
          <xsl:value-of select="following-sibling::node()/key[14]/following-sibling::node()/text()"/>
        </xsl:element>
        <xsl:element name="ReleaseDate">
          <xsl:value-of select="following-sibling::node()/key[16]/following-sibling::node()/text()"/>
        </xsl:element>
        <xsl:element name="Coments"></xsl:element>
        <xsl:element name="Shares"></xsl:element>
      </xsl:element>
    </xsl:if>
  </xsl:template>
 
</xsl:stylesheet>
