xquery version "1.0";

declare namespace response="http://exist-db.org/xquery/response";
import module namespace request="http://exist-db.org/xquery/request";
import module namespace session="http://exist-db.org/xquery/session";
import module namespace util="http://exist-db.org/xquery/util";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";



declare function local:setURL() as xs:anyURI
{
  let $a as xs:string := request:get-parameter("user",""),
      $b as xs:string := request:get-parameter("musicid",""),
  $sa as xs:string :=
    if ($a)
      then concat("&amp;user=",$a)
      else ""

  return
    xs:anyURI(concat("music.xql?musicID=",$b,$sa))
};
declare variable $redirect-uri as xs:anyURI := local:setURL();

let $collection := 'xmldb:exist:///db/itunes'
let $login := xmldb:login($collection, 'admin', '1234')

let $mid := request:get-parameter("musicid",""),
    $user := request:get-parameter("user",())
let $doc := doc(concat('/db/itunes/',$user,'/musics.xml'))/Musics/Music[@musicID = $mid]

let $comDoc := doc('/db/itunes/coments.xml')

let $comId := concat("com",count($comDoc/Coments/Coment))
let $com := 
  <Coment comentID="{$comId}">
    <Text>{request:get-parameter("comment","")}</Text>
    <Autor>{ data(corba:main()/@userID) }</Autor>
  </Coment>
let $bla := update insert $com into $comDoc/Coments

let $cm :=
  <Coment>{$comId}</Coment>
let $ble := update insert $cm into $doc/Coments

return 
  response:redirect-to($redirect-uri)




