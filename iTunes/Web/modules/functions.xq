xquery version "1.0";

module namespace corba="http://www.corbamusic.com/functions";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace math="http://exist-db.org/xquery/math";

declare function corba:main() as node()?
{
    let $user-id := session:get-attribute("user"),
        $guess := doc("/db/itunes/users.xml")/Users/User[@userID = $user-id]
        return
    if ($guess) then
      $guess
    else
      ()
};

declare function corba:username() as xs:string
{
  let $username := xs:string(data(corba:main()/Attributes/NomeCompleto))
  return
    if($username)
      then $username
      else "Guest"
};

declare function corba:get-user($user-id as xs:string) as node()?
{
    let $guess := doc("/db/itunes/users.xml")/Users/User[@userID = $user-id]
        return
    if ($guess) then
      $guess
    else
      <p>No input!</p>
};

declare function corba:timeMusic($time as xs:double) as element()
{
  let $mili := $time mod 1000,
      $sec := math:floor(($time div 1000) mod 60),
      $min := math:floor($time div 60000)

  let $sec_st := 
    if($sec < 10)
      then concat("0",$sec)
      else $sec
  return
    <value>{$min}:{$sec_st}</value>
};

declare function corba:fetchPlaylists() as element()*
{
  let $user-id := session:get-attribute("user")
  return
  if($user-id) then
  let $guess := doc("/db/itunes/users.xml")/Users/User[@userID = $user-id]
  for $each in $guess/Playlists/PlaylistID
	let $playlist := doc(concat('/db/itunes/',$user-id,'/playlists.xml'))/Playlists/Playlist[@playlistID = data($each)]
	  return
		if ($playlist) then
		  <li>
		  <a href="playlist.xql?playlistID={data($playlist/@playlistID)}&amp;user={data($playlist/@owner)}" title="{data($playlist/Purpose)}">{data($playlist/Name)}</a>
		  </li>
		else ()
  else 
    for $playlist in doc('/db/itunes/Guest/playlists.xml')/Playlists/Playlist
    return
        <li>
        <a href="playlist.xql?playlistID={data($playlist/@playlistID)}&amp;user=Guest" title="{data($playlist/Purpose)}">{data($playlist/Name)}</a>
        </li>
};

declare function corba:fetchAlbums() as element()*
{
  let $user-id := session:get-attribute("user")
  let $doc := doc(concat('/db/itunes/',$user-id,'/playlists.xml'))/Playlists/Playlist[@kind = "Album"]
  for $playlist in $doc
  return
      <li>
      <a href="playlist.xql?playlistID={data($playlist/@playlistID)}" title="{data($playlist/Purpose)}">{data($playlist/Name)}</a>
      </li>
};

declare function corba:fetchFriends() as element()*
{
  let $user-id := session:get-attribute("user"),
      $me := doc("/db/itunes/users.xml")/Users/User[@userID = $user-id]
      for $id in $me/Following/User
      for $user in doc("/db/itunes/users.xml")/Users/User[@userID = $id]
      return
        <tr id="tableLink" onclick="DoNav('user.xql?user={data($user/@userID)}');">
        <td><img src="{data($user/Attributes/Foto)}" height="40" width="40"/></td>
        <td>{data($user/Attributes/NomeUtilizador)}</td>
        </tr>
};


declare function corba:fetchMusics() as element()*
{
  let $ruser := request:get-parameter("user",()),
      $user := if($ruser) then $ruser else "Guest"
  let $playID := request:get-parameter("playlistID",()),
      $playlists := doc(concat("/db/itunes/",$user,"/playlists.xml"))/Playlists/Playlist[@playlistID = $playID]
  for $music in $playlists/Musics/MusicID
  let $play := doc(concat("/db/itunes/",$user,"/musics.xml"))/Musics/Music[@musicID = $music]
    return
      <tr id="tableLink" onclick="DoNav('music.xql?musicID={data($play/@musicID)}&amp;user={data($play/@owner)}');">
      <td>{data($play/Title)}</td>
      <td>{corba:timeMusic(data($play/Time))}</td>
      <td>{data($play/Genre)}</td>
      <td>{data($play/Rating)}</td>
      <td>{data($play/BeatRate)} bpm's</td>
      <td>{data($play/BitRate)} KHz</td>
      <td>{data($play/ReleaseDate)}</td>
      </tr>
};

declare function corba:fetchMusic() as element()?
{
  let $music := request:get-parameter("musicID",()),
      $ms_user := request:get-parameter("user",())
  let $musicInfo := doc(concat('/db/itunes/',$ms_user,'/musics.xml'))/Musics/Music[@musicID = $music]
  return
      <ul style='list-style-type: none;'>
        <li>Name:
          <span>{data($musicInfo/Title)}</span>
        </li>
        <li>Author:
          <span>{data($musicInfo/Author)}</span>
        </li>
        <li>Time:
          <span>{corba:timeMusic($musicInfo/Time)}</span>
        </li>
        <li>Genre:
          <span>{data($musicInfo/Genre)}</span>
        </li>
        {
        if(data($musicInfo/Rating))
        then 
        <li>Rating:
          <span>{data($musicInfo/Rating)}</span>
        </li>
        else ()
        }{
        if(data($musicInfo/BeatRate))
        then <li>BeatRate:
          <span>{data($musicInfo/BeatRate)} bpm's</span>
        </li>
        else ()
        }{
        if(data($musicInfo/BitRate))
        then <li>Bit Rate:
          <span>{data($musicInfo/BitRate)} KHz</span>
        </li>
        else ()
        }{
        if(data($musicInfo/ReleaseDate))
        then 
        <li>Release Date:
          <span>{data($musicInfo/ReleaseDate)}</span>
        </li>
        else ()
        }
{
        if(corba:username()!='Guest') then
        <li>
          <button id="linkUpdateMusic">Update Music</button>
          <button id="linkAddComment">Add Comment</button>
          <div id="div_form" style="display:none; margin-left:100px">
            <p>Write your comment:</p>
            <form action="addcomment.xql" method="get">
              <textarea id="ta_com" name="comment" rows="5" cols="40"></textarea>
              <input type="hidden" name="user" value="{$ms_user}"/>
              <input type="hidden" name="musicid" value="{$music}"/>
              <br/>
              <button id="formCom_cancel">Cancel</button>
              <input type="submit" value="Submit"/>
            </form>
          </div>
        </li>
        else ()
}
        {
         if(count($musicInfo/Coments/Coment) gt 0 )
          then
           <div>
             <li>Comments:</li>
             <ul>
               {
               for $comid in $musicInfo/Coments/Coment
               let $com := doc('/db/itunes/coments.xml')/Coments/Coment[@comentID=$comid]
               let $cuser := doc('/db/itunes/users.xml')/Users/User[@userID=$com/Autor]/Attributes/NomeCompleto
               return
                 <li>{data($cuser)}: {data($com/Text)}</li>

               }
             </ul>
           </div>
          else ()
        }
      </ul>
};

declare function corba:fetchAllMusics() as element()*
{
  let $user-id := session:get-attribute("user"),
      $page := xs:integer(request:get-parameter("page",0))
  let $playID := request:get-parameter("playlistID",())
  for $play in collection("/db/itunes")/Musics/Music[position() gt $page*20][position() lt 21]
          return
            <tr id="tableLink" onclick="DoNav('music.xql?user={data($play/@owner)}&amp;musicID={data($play/@musicID)}');">
            <td>{data($play/Title)}</td>
            <td>{corba:timeMusic(xs:double($play/Time))}</td>
            <td>{data($play/Genre)}</td>
            <td>{data($play/Rating)}</td>
            <td>{data($play/BeatRate)} bpm's</td>
            <td>{data($play/BitRate)} KHz</td>
            <td>{data($play/ReleaseDate)}</td>
            </tr>
};


declare function corba:fetchMyMusics() as element()*
{

  let $user-id := session:get-attribute("user"),
      $page := xs:integer(request:get-parameter("page",0)),
      $doc := doc(concat('/db/itunes/',$user-id,'/musics.xml'))
  for $play in $doc/Musics/Music[position() gt $page*20][position() lt 21]
    return
      <tr id="tableLink" onclick="DoNav('music.xql?user={data($play/@owner)}&amp;musicID={data($play/@musicID)}');">
      <td>{data($play/Title)}</td>
      <td>{corba:timeMusic(xs:double($play/Time))}</td>
      <td>{data($play/Genre)}</td>
      <td>{data($play/Rating)}</td>
      <td>{data($play/BitRate)} KHz</td>
      <td>{data($play/ReleaseDate)}</td>
      </tr>
};

declare function corba:pagination($max as xs:integer, $href as xs:string) as element()
{
  let $page := xs:integer(request:get-parameter("page",0))
  return
    if ($max gt 0)
    then
      <span>
        {if ($page gt 0) then 
          <a href="/exist/itunes/{$href}?page={$page -1}">Previous</a>
        else ()
        }{
        if ($page lt $max) then 
          <a href="/exist/itunes/{$href}?page={$page +1}">Next</a>
        else ()}
      </span>
    else <span/>
};

declare function corba:simpleSearch($query as xs:string) as element()
{
   <div>
    <h2>Musics:</h2>
    {
    let $rstmusic := collection('/db/itunes')/Musics/Music[ft:query(.,$query)]
    return
      if($rstmusic)
      then
        for $m in $rstmusic
          return
            <div class='hit'>
              <a href="music.xql?musicID={$m/@musicID}">
              {data($m/Title)},{data($m/Author)}
              </a>
            </div>
     else <span>No results.</span>
    }
  </div>


};
