xquery version "1.0";

module namespace skel="http://www.corbamusic.com/skel";

import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";


declare function skel:main_head($content as element()*) as element()*
{
  let $a :=
  <head>
    <title>Cobra Music Share</title>
    <link rel="shortcut icon" href="images/logo.png"/>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <script type="text/javascript" src="js/jquery.js" language="javascript"></script>
    <script type="text/javascript" src="js/global.js" language="javascript"></script>
    { $content }
  </head>

  return $a
};

declare function skel:main_body($content as element()*) as element()*
{
  let $a :=
    <body>
    <table>
      <tr>
        <td id="header">
          <div id="topSection">
            <div id='user_area'>
              <a href="user.xql">Guest User</a>
              <button id='bt_login' type='button'>Log in</button>
              <button id='bt_register' type='button'>Register</button>
            </div>
          </div>
          <div>
            <div id='logo'>
              <a href='index.html'>
                <img src="images/logo.png" alt="Logo" />
              </a>
            </div>
            <div id='appname'>
              Cobra Music Share
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td id="content">
            { $content }
        </td>
      </tr>
      <tr id="footer">
        <td>
          <div id="authors">Constantino Gomes &amp; Luis Malhadas</div>
          <div id="copyRight"> Cobra Music Share &#169; 2012 - FCT </div>
          <div id="validators">
            <a href="#"><img src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
            <a href="#"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS" height="31" width="88" /></a>
          </div>
        </td>
      </tr>
    </table>
    </body>

  return $a
};


declare function skel:user_head($content as element()?) as element()*
{
  let $a :=
  <head>
    <title>{corba:username()}'s Home Page</title>
    <link rel="stylesheet" href="stylesheets/user.css"/>
    <script type="text/javascript" src="js/jquery.js" language="javascript"></script>
    <script type="text/javascript" src="js/global.js" language="javascript"></script>
    <script type="text/javascript" src="js/clickableTable.js" language="javascript"></script>
    {if($content) then
      <script type="text/javascript" src="js/{data($content)}" language="javascript"></script>
    else () }
  </head>

  return $a
};

declare function skel:user_body($username as xs:string?, $content as element()*) as element()*
{
  let $a :=
    <body>
    <div id='header'>
      <div style='padding-top:10px;'>
        <div style='float:left;height:26px;padding-left:10px;'>
          <table>
            <tr>
              <td style='width: 25px;'>
                <a href="user.xql">
            <img src="{corba:main()/Attributes/Foto}" alt="user" height="24" width="24"/>
                </a>
              </td>
              <td style='color:white;'>
                <a href="user.xql">
                {$username}
                </a>
              </td>
            </tr>
          </table>
        </div>
        <div style='float:right;'>
          {if(corba:username()!='Guest') then
          <button id='bt_upload' type='button'>Upload Library</button>
           else ()}
          <button id='bt_logout' type='button'>Log Out</button>
        </div>
        <div id="quick_search" style='float:right;margin: 0px 20px'>
          <input id="searchtext" type="text" />
          <button id="bt_search" type='button'>Search</button>
        </div>
      </div>
    </div>
    <span id="main">
      <div id='menu'>
        <ul>
          <li><a href="AllMusics.xql"><span>All Musics</span></a></li>
          {if(corba:username()!='Guest') then
          <li><a href="myMusics.xql"><span id="selected">My Musics</span></a></li>
            else ()}
          <li><span>Albums</span>
            <div style="max-height:200px;display:block;overflow:auto;">
              <ul>
                {corba:fetchAlbums()}
              </ul>
            </div>
          </li>
          <li><span>Playlists</span>
            <div style="max-height:200px;display:block;overflow:auto;">
              <ul>
                {corba:fetchPlaylists()}
              </ul>
            </div>
          </li>
        </ul>
        {if(corba:username()!='Guest') then
        <div>
          <hr/>
          <ul>
            <li><a href="playlistForm.xql">Create Playlist</a></li>
            <li><a href="playlistShare.xql">Share Playlist</a></li>
          </ul>
          <hr/>
          <ul>
            <li><a href="friends.xql"><span>Friends</span></a></li>
            <li><a href="friends_add.xql">Add Friends</a></li>
          </ul>
        </div>
        else ()
      }
      </div>
      { $content }
    </span>
    <div id='footer'>
      <div id="authors">Constantino Gomes &amp; Luis Malhadas</div>
      <div id="copyRight"> Cobra Music Share &#169; 2012 - FCT </div>
      <div id="validators">
        <a href="#"><img src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
        <a href="#"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS" height="31" width="88" /></a>
      </div>

    </div>
    </body>

  return $a
};




