xquery version "1.0";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace session="http://exist-db.org/xquery/session";
declare namespace xdb="http://exist-db.org/xquery/xmldb";
declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

declare function local:do-update() as element()?
{
	let $collection := 'xmldb:exist:///db/itunes/'
	let $login := xmldb:login($collection, 'admin', '1234')

  let $music := request:get-parameter("musicID",()),
      $ms_user := request:get-parameter("user",())
  let $musicInfo := doc(concat('/db/itunes/',$ms_user,'/musics.xml'))/Musics/Music[@musicID = $music]

  let $name := request:get-parameter("name", ()),
		$author := request:get-parameter("author", ()),
		$time := request:get-parameter("time", ()),
		$genre := request:get-parameter("genre", ()),
		$rating := request:get-parameter("rating",()),
		$beatrate := request:get-parameter("beatrate",()),
    $bitrate := request:get-parameter("bitrate",()),
    $releasedate := request:get-parameter("releasedate",())
		return
      if(boolean('true')) then (
					if ($name) then
						update value $musicInfo/Title with string($name)
					else(),
          if ($author) then
            update value $musicInfo/Author with string($author)
          else(),
          if ($time) then
            update value $musicInfo/Time with string($time)
          else(),
          if ($genre) then
            update value $musicInfo/Genre with string($genre)
          else(),
          if ($rating) then
            update value $musicInfo/Rating with string($rating)
          else(),
          if ($beatrate) then
            update value $musicInfo/BeatRate with string($beatrate)
          else(),
          if ($bitrate) then
            update value $musicInfo/BitRate with string($bitrate)
          else(),
          if ($releasedate) then
            update value $musicInfo/ReleaseDate with string($releasedate)
          else()
      ) else ()
};
declare function local:formMusic() as element()?
{
  let $music := request:get-parameter("musicID",()),
      $ms_user := request:get-parameter("user",())
  let $musicInfo := doc(concat('/db/itunes/',$ms_user,'/musics.xml'))/Musics/Music[@musicID = $music]
  return
    <form action="updateMusicForm.xql" method="get">
      <fieldset>
          <legend>Update Music</legend>
          Name:
          <br/>
          <input type="text" name="name" value="{data($musicInfo/Title)}"/>
          <br/>
          Author: 
          <br/>
          <input type="text" name="author" value="{data($musicInfo/Author)}"/>
          <br/>
          Time (ms): 
          <br/>
          <input type="text" name="time" value="{data($musicInfo/Time)}"/>
          <br/>
          Genre: 
          <br/>
          <input type="text" name="genre" value="{data($musicInfo/Genre)}"/>
          <br/>
          Rating: 
          <br/>
          <input type="text" name="rating" value="{data($musicInfo/Rating)}"/>
          <br/>
          BeatRate (bpm's): 
          <br/>
          <input type="text" name="beatrate" value="{data($musicInfo/BeatRate)}"/>
          <br/>
          Bit Rate (KHz): 
          <br/>
          <input type="text" name="bitrate" value="{data($musicInfo/BitRate)}"/>
          <br/>
          Release Date: 
          <br/>
          <input type="text" name="releasedate" value="{data($musicInfo/ReleaseDate)}"/>
          <br/>
          <div id="div_form">
              <input type="hidden" name="user" value="{$ms_user}"/>
              <input type="hidden" name="musicid" value="{$music}"/>
              <br/>
              <button id="formMusic_cancel">Cancel</button>
              <input type="submit" value="Submit"/>
          </div>
        </fieldset>
      </form>
};

let $body :=
  <div id="content">
    {local:do-update()}
    {local:formMusic()}
  </div>


let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html
