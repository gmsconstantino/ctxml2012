xquery version "1.0";

import module namespace request="http://exist-db.org/xquery/request";
import module namespace session="http://exist-db.org/xquery/session";
import module namespace util="http://exist-db.org/xquery/util";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

import module namespace corba="http://www.corbamusic.com/functions" at "http://localhost:8080/exist/itunes/modules/functions.xq";

declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";


let $friend := request:get-parameter("friend",())
let $body :=
<div id="content">
  <h1>Add Friends.</h1>
  <form action="friends_add.xql" method="get">
    <fieldset>
      <legend>Search Friend:</legend>
      <p>You can search by Name, Email or Username</p>
      <p>If the search result on a single user, he will be added to your friends.</p>
      <input type="text" name="friend"/>
      <input type="reset" value="Reset"/>
      <input id="searchF" type="submit" value="Submit"/>
    </fieldset>
  </form>
{
  if($friend) then 
    let $user-id := session:get-attribute("user")
    let $search := doc('/db/itunes/users.xml')/Users/User[ft:query(Attributes,$friend)]
    let $r :=
      if(count($search)>1)
      then
        <div><ul>{
          for $u in $search
          return
            <li>{data($u/Attributes/NomeUtilizador)}, {data($u/Attributes/NomeCompleto)}, {data($u/Attributes/Email)}</li>
        }</ul></div>
      else if(count($search)=1) then
          let $a := update insert <User>{data($search/@userID)}</User> into doc('/db/itunes/users.xml')/Users/User[@userID=$user-id]/Following
          return
            <p>You add successfully a friend.</p>
      else <p>No results.</p>
     return $r
  else ()
}
</div>

let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:user_head(())}
{skel:user_body(corba:username(),$body)}
</html>

return
  $html

