xquery version "1.0";
import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

let $body :=
    <div>
      <div id="best">
        <p>The Best Library Sharing Site</p>
        <table>
          <tr>
            <td>
                Cobra Music Share gives you access to over 1 milion information of songs and playlists,
                all legal and free! Discover new artists and share with friends.
            </td>
            <td>
              <img src="images/interface.png" alt="imagem da app" />
            </td>
          </tr>
        </table>
      </div>
      <div id="features">
        <p>About &amp; Features</p>
        <ul>
          <li><span>Simple and Advanced Search</span></li>
          <li><span>Browsing Information</span></li>
          <li><span>Create and Share your Playlists</span></li>
          <li><span>Comment and evaluate the musics</span></li>
          <li><span>Personalize the enviroment</span></li>
        </ul>
      </div>
      <div id='award'>
        <!--<img src="images/award.png" alt="award softpedia" />-->
      </div>
    </div>

let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:main_head(())}
{skel:main_body($body)}
</html>

return
  $html
