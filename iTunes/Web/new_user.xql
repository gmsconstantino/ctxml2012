xquery version "1.0";
import module namespace request="http://exist-db.org/xquery/request";
import module namespace session="http://exist-db.org/xquery/session";
import module namespace util="http://exist-db.org/xquery/util";

import module namespace skel="http://www.corbamusic.com/skel" at "http://localhost:8080/exist/itunes/modules/skelpages.xq";

declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

declare option exist:serialize "method=xhtml media-type=text/html omit-xml-declaration=no indent=yes 
  doctype-public=-//W3C//DTD&#160;XHTML&#160;1.1//EN
  doctype-system=http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd";

declare function local:random($max as xs:integer) as xs:integer
{
    let $r := ceiling(util:random() * $max) cast as xs:integer
    return $r
};


let $parameters :=  request:get-parameter-names()

let $collection := 'xmldb:exist:///db/itunes'
let $login := xmldb:login($collection, 'admin', '1234')

let $userid := concat('user',local:random(1000000))
let $newu_col := xmldb:create-collection($collection, $userid)

let $newu_db := xmldb:create-user($userid, request:get-parameter('password', ''), "dba", concat("/db/itunes/",$userid))

(:let $result :=:)
(:<results>:)
   (:<random>{$userid}</random>:)
   (:<parameters>{$parameters}</parameters>:)
   (:{for $parameter in $parameters:)
   (:return:)
   (:<parameter>:)
      (:<name>{$parameter}</name>:)
      (:<value>{request:get-parameter($parameter, '')}</value>:)
   (:</parameter>:)
   (:}:)
(:</results>:)
(:let $store := xmldb:store($newu_col, 'text.xml', $result):)

let $useratt :=
  <User userID="{$userid}">
      <Attributes>
          <NomeCompleto>{request:get-parameter('firstname', '')}&#032;{request:get-parameter('lastname', '')}</NomeCompleto>
          <NomeUtilizador>{request:get-parameter('username', '')}</NomeUtilizador>
          <DataNascimento>{request:get-parameter('year', '')}-{request:get-parameter('month', '')}-{request:get-parameter('day', '')}</DataNascimento>
          <DataRegisto>{current-date()}</DataRegisto>
          <Email>{request:get-parameter('email', '')}</Email>
          <Foto>https://secure.gravatar.com/avatar/49d85eca1cebd6245348f1acf8d8d30b?d=https%3A%2F%2Fdwz7u9t8u8usb.cloudfront.net%2Fm%2F2219cd120e6a%2Fimg%2Fdefault_avatar%2F32%2Fuser_blue.png&amp;s=32</Foto>
          <Password>{request:get-parameter('password', '')}</Password>
          <Estado>A</Estado>
      </Attributes>
      <Playlists>
      </Playlists>
      <Following>
      </Following>
  </User>

let $doc := doc('/db/itunes/users.xml')
let $bla := update insert $useratt into $doc/Users

let $ble := xmldb:copy("/db/itunes",concat("/db/itunes/",$userid),"musics_user.xml")
let $ble2 := xmldb:rename(concat("/db/itunes/",$userid),"musics_user.xml","musics.xml")
let $ble3 := xmldb:set-resource-permissions(concat("/db/itunes/",$userid),"musics.xml",$userid,"dba",509)
let $bli := xmldb:copy("/db/itunes",concat("/db/itunes/",$userid),"playlists_user.xml")
let $bli2 := xmldb:rename(concat("/db/itunes/",$userid),"playlists_user.xml","playlists.xml")
let $bli3 := xmldb:set-resource-permissions(concat("/db/itunes/",$userid),"playlists.xml",$userid,"dba",509)


let $body :=
  <div>{
    if ($newu_col)
    then (
      <span>Registo Completo</span>
    ) else (
      <span>Registo Falhado</span>
    )
  }
  <br/>
  <table>
    <tr>
      <td>
        <a href="/exist/itunes/login.xql">Login</a>
      </td>
      <td>
        <a href="/exist/itunes">Voltar</a>
      </td>
    </tr>
  </table>
  </div>


let $html :=
<html xmlns="http://www.w3.org/1999/xhtml">
{skel:main_head(())}
{skel:main_body($body)}
</html>

return
  $html



