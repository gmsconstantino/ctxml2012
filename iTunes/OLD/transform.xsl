<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" doctype-system="itunes.dtd"/>


  <xsl:template match="/">
    <keys>
      <xsl:apply-templates select="plist"/>
    </keys>
  </xsl:template>

  <xsl:template match="plist">
    <xsl:apply-templates select="*"/>
  </xsl:template>


  <xsl:template match="key">
      <xsl:copy-of select="key" />
  </xsl:template>

</xsl:stylesheet>
