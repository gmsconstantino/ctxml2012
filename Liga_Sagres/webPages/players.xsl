<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title></title>
       <link rel="stylesheet" type="text/css" href="css/jogadores.css" media="all"/>
      </head>
      <body>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="LigaSagres">
    <xsl:apply-templates select='Pessoas'/>
  </xsl:template>

  <xsl:template match="Pessoas">
    <table>
      <tr>
        <th>Nome</th>
        <th>Nick</th>
        <th>Nacionalidade</th>
        <th>Clube</th>
        <th>Golos Marcados</th>
        <th>Jogos</th>
        <th>Jogos Titular</th>
      </tr>
      <xsl:for-each select="Pessoa[@tipo = 'Jogador']">
        <tr>
          <td>
            <xsl:value-of select="Nome" />
          </td>
          <td>
            <xsl:value-of select="Nick" />
          </td>
          <td>
            <xsl:value-of select="Pais" />
          </td>
          <td>
            <xsl:value-of select="Clube" />
          </td>
          <td>
            <xsl:value-of select="Golos" />
          </td>
          <td>
            <xsl:value-of select="Jogos" />
          </td>
          <td>
            <xsl:value-of select="Titular" />
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

</xsl:stylesheet>
