function loadXMLDoc(dname)
{
  if (window.XMLHttpRequest)
    {
      xhttp=new XMLHttpRequest();
    }
    else
      {
        xhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xhttp.open("GET",dname,false);
      xhttp.send("");
      return xhttp.responseXML;
}

var xml=loadXMLDoc("data2.xml");

$(function() {


  $('#mEstadio').click(function(){
    var xsltProcessor=new XSLTProcessor();
    var xsl=loadXMLDoc("estadios.xsl");
    xsltProcessor.importStylesheet(xsl);
    var resultDocument = xsltProcessor.transformToFragment(xml,document);
    document.getElementById("content").innerHTML = '';
    document.getElementById("content").appendChild(resultDocument);
  });

  $('#mClassificacao').click(function(){
    var xsltProcessor=new XSLTProcessor();
    var xsl=loadXMLDoc("classificacao.xsl");
    xsltProcessor.importStylesheet(xsl);
    var resultDocument = xsltProcessor.transformToFragment(xml,document);
    document.getElementById("content").innerHTML = '';
    document.getElementById("content").appendChild(resultDocument);
  });

  $('#mClubes').click(function(){
    var xsltProcessor=new XSLTProcessor();
    var xsl=loadXMLDoc("clubes.xsl");
    xsltProcessor.importStylesheet(xsl);
    var resultDocument = xsltProcessor.transformToFragment(xml,document);
    document.getElementById("content").innerHTML = '';
    document.getElementById("content").appendChild(resultDocument);

    $('.linkClube').click(function(){
      var link = $(this).attr('rel');

      var xsltProcessor=new XSLTProcessor();
      var xsl=loadXMLDoc("clube.xsl");
      xsltProcessor.setParameter(null,"selclube", link);
      xsltProcessor.importStylesheet(xsl);
      var resultDocument = xsltProcessor.transformToFragment(xml,document);
      document.getElementById("rightSide").innerHTML = '';
      document.getElementById("rightSide").appendChild(resultDocument);
    })

  });

  $('#mJogadores').click(function(){
    var xsltProcessor=new XSLTProcessor();
    var xsl=loadXMLDoc("players.xsl");
    xsltProcessor.importStylesheet(xsl);
    var resultDocument = xsltProcessor.transformToFragment(xml,document);
    document.getElementById("content").innerHTML = '';
    document.getElementById("content").appendChild(resultDocument);
  });

  $('#mJornada').click(function(){
    var xsltProcessor=new XSLTProcessor();
    var xsl=loadXMLDoc("gameweeks.xsl");
    xsltProcessor.importStylesheet(xsl);
    var resultDocument = xsltProcessor.transformToFragment(xml,document);
    document.getElementById("content").innerHTML = '';
    document.getElementById("content").appendChild(resultDocument);


    $('.linkJornada').click(function(){
      var link = $(this).attr('rel');

      var xsltProcessor=new XSLTProcessor();
      var xsl=loadXMLDoc("gameweek.xsl");
      xsltProcessor.setParameter(null,"seljornada", link);
      xsltProcessor.importStylesheet(xsl);
      var resultDocument = xsltProcessor.transformToFragment(xml,document);
      document.getElementById("rightSide").innerHTML = '';
      document.getElementById("rightSide").appendChild(resultDocument);
    })
  });

  $('#mJogador').click(function(){
    var xsl=loadXMLDoc("searchplayer.xsl");
    var xsltProcessor=new XSLTProcessor();
    xsltProcessor.importStylesheet(xsl);

    var resultDocument = xsltProcessor.transformToFragment(xml,document);
    document.getElementById("content").innerHTML = '';
    document.getElementById("content").appendChild(resultDocument);
  });

});

function searchPlayerForm(event){
  var xsl=loadXMLDoc("player.xsl");
  var searchTerm = $("#search input").eq(0).val();

  xsltProcessor = new XSLTProcessor();
  xsltProcessor.setParameter(null,"searchTerm",searchTerm);
  xsltProcessor.importStylesheet(xsl);

  var resultDocument = xsltProcessor.transformToFragment(xml,document);
  document.getElementById("contentPlayer").innerHTML = '';
  document.getElementById("contentPlayer").appendChild(resultDocument);
  return false;
}
