<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title></title>
        <script type="text/javascript" src="js/script.js"></script>
      </head>
      <body>
        <form id="search">
          <input type="text" value="Nome do Jogador..." name="searchTerm"/>
          <input type="button" value="search" onclick="searchPlayerForm()"/>
        </form>
        <div id="contentPlayer">
        </div>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
