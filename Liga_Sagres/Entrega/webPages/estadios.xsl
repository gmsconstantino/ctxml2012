<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>		
      <link rel="stylesheet" type="text/css" href="css/estadios.css" media="all"/>
        <title></title>
      </head>
      <body>
        <table>
          <tr>
            <th>Estádio</th>
            <th>Número de Jogos</th>
            <th>Total de Espectadores</th>
          </tr>
          <xsl:for-each select="LigaSagres/Estadios/Estadio">
            <tr>
              <td><xsl:value-of select="Nome" /></td>
              <td><xsl:value-of select="count(Jogos/nEspectadores)" /></td>
              <td><xsl:value-of select="sum(Jogos/nEspectadores)" /></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
