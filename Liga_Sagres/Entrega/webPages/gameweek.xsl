<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
  <xsl:output method="html" encoding="UTF-8"/>

  <xsl:param name="seljornada" select="ola"/>

  <xsl:template match="/">
    <html>
      <head>
        <title></title> 
        <link rel="stylesheet" type="text/css" href="css/jornada.css" media="all"/>
      </head>
      <body>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="LigaSagres">
    <xsl:apply-templates select='Epocas'/>
  </xsl:template>

  <xsl:template match="Epocas">
    <xsl:variable name="jornada" select="Epoca/Jornada[@id = $seljornada]"/>

    <p>Jornada: <span> <xsl:value-of select="substring(exsl:node-set($jornada)/@id,3)" /> </span></p>
    <p>Numero de Jogos: <span> <xsl:value-of select="exsl:node-set($jornada)/Estatisticas/NJogos" /> </span></p>

    <table>
      <tr>
        <th>Clube Casa</th>
        <th>Resultado</th>
        <th>Clube Visitante</th>
      </tr>
      <xsl:for-each select="exsl:node-set($jornada)/Jogo">
        <xsl:variable name="casa" select='count(GolosCasa/Golo)'/>
        <xsl:variable name="visit" select='count(GolosVisitante/Golo)'/>
        <tr>
          <td><xsl:value-of select="ClubeCasa" /></td>
          <td><xsl:value-of select="$casa" /><xsl-text> - </xsl-text><xsl:value-of select="$visit" />
          </td>
          <td><xsl:value-of select="ClubeVisitante" /></td>
        </tr>
      </xsl:for-each>
    </table>

    <p>Numero de Vitorias: <span> <xsl:value-of select="exsl:node-set($jornada)/Estatisticas/Vitorias" /></span></p>
    <p>Numero de Empate: <span> <xsl:value-of select="exsl:node-set($jornada)/Estatisticas/Empates" /></span></p>
  </xsl:template>

</xsl:stylesheet>
