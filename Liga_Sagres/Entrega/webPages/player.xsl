<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/player.css" media="all"/>
        <script type="text/javascript" src="js/script.js"></script>
      </head>
      <body>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="LigaSagres">
    <xsl:apply-templates select='Pessoas'/>
  </xsl:template>

  <xsl:param name="searchTerm"/>
  <xsl:template match="Pessoas">
    <xsl:for-each select="Pessoa[Nome = $searchTerm]">
      <div>
        <p>Nome do Jogador: </p> <span><xsl:value-of select="Nome" /></span>
        <p>Nick: </p><span><xsl:value-of select="Nick" /></span>
        <p>Clube: </p><span><xsl:value-of select="Clube" /></span>
        <p>Nacionalidade: </p><span><xsl:value-of select="Pais" /></span>
        <p>Numero de Golos: </p><span><xsl:value-of select="Golos" /></span>
        <p>Numero de vezes Titular: </p><span><xsl:value-of select="Titular" /></span>
      </div>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
