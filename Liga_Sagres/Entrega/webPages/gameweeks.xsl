<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
  <xsl:template match="/">
    <html>
      <head>
        <title></title> 
        <link rel="stylesheet" type="text/css" href="css/jornada.css" media="all"/>
      </head>
      <body>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="LigaSagres">
    <div class="leftSide">
      <ul>
        <xsl:for-each select="Epocas/Epoca/Jornada">
        <li>
          <a href="#" class="linkJornada">
            <xsl:attribute name="rel">
              <xsl:value-of select="@id" />
            </xsl:attribute>
            <xsl:text>Jornada </xsl:text><xsl:value-of select="position()" />
          </a>
        </li>
      </xsl:for-each>
      </ul>
    </div>
    <div id="rightSide">
    </div>
  </xsl:template>


</xsl:stylesheet>
