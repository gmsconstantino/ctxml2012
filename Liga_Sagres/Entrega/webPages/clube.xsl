<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="UTF-8"/>

  <xsl:param name="selclube" select="ola"/>

  <xsl:template match="/">
    <html>
      <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/clubes.css" media="all"/>
      </head>
      <body>
      <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="LigaSagres">
    <xsl:apply-templates select="Clubes"/>
  </xsl:template>

  <xsl:template match="Clubes">
    <xsl:for-each select="Clube[@id = $selclube]">
      <div>
        <p>Nome do Clube: <span> <xsl:value-of select="Nome" /></span></p>
        <p>Sigla: <span> <xsl:value-of select="Sigla" /></span></p>
        <p id="info"> <a> <xsl:attribute name="href"> <xsl:value-of select="Website"/> </xsl:attribute> Site oficial</a></p>
        <p>Descricao: <span> <xsl:value-of select="Descricao" /></span></p>

        <xsl:for-each select="EqTecnica">
          <p>
            <xsl:value-of select="Membro/@cargo" />
            <xsl:text>: </xsl:text>
            <span>
              <xsl:value-of select="Membro/Nome" />
            </span>
          </p>
        </xsl:for-each>


        <div>
          <p>Guarda-Redes:</p>
          <ul>
            <xsl:for-each select="Equipa/GuardaRedes">
              <li>
                <!--<xsl:value-of select="@jogadorRef" />-->
                <xsl:value-of select="text()" />
              </li>
            </xsl:for-each>
          </ul>
          <p>Defesas:</p>
          <ul>
            <xsl:for-each select="Equipa/Defesa">
              <li>
                <!--<xsl:value-of select="@jogadorRef" />-->
                <xsl:value-of select="text()" />
              </li>
            </xsl:for-each>
          </ul>
          <p>Medios:</p>
          <ul>
            <xsl:for-each select="Equipa/Medio">
              <li>
                <!--<xsl:value-of select="@jogadorRef" />-->
                <xsl:value-of select="text()" />
              </li>
            </xsl:for-each>
          </ul>
          <p>Avancados:</p>
          <ul>
            <xsl:for-each select="Equipa/Avancado">
              <li>
                <xsl:value-of select="text()" />
                <!--<xsl:value-of select="@jogadorRef" />-->
              </li>
            </xsl:for-each>
          </ul>
        </div>

        <p>Premios Alcancados:</p>
        <ul>
          <xsl:for-each select="Historico/Classificacao">
            <li>
              <xsl:value-of select="Designacao" /> 
              <xsl:text> </xsl:text>
              <xsl:value-of select="Ano" />
            </li>
          </xsl:for-each>
        </ul>


      </div>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
