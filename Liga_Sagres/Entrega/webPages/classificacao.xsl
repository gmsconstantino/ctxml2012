<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title></title>
        		<link rel="stylesheet" type="text/css" href="css/classificacao.css" media="all"/>
      </head>
      <body>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="LigaSagres">
    <xsl:apply-templates select='Clubes'/>
  </xsl:template>

  <xsl:template match="Clubes">
    <table>
      <tr>
        <th>Nome</th>
        <th>Pontos</th>
        <th>Numero Jogos</th>
        <th>Vitorias</th>
        <th>Empates</th>
        <th>Derrotas</th>
        <th>Nº Golos Marcados</th>
        <th>NºGolos Sofridos</th>
        <th>Media Golos Marcados</th>
        <th>Media Golos Sofridos</th>
      </tr>
      <xsl:for-each select="Clube">
        <xsl:sort select="Estatisticas/Pontos" data-type="number" order="descending"/>
        <tr>
          <td><xsl:value-of select="Nome" /></td>
          <td><xsl:value-of select="Estatisticas/Pontos" /></td>
          <td><xsl:value-of select="Estatisticas/NJogos" /></td>
          <td><xsl:value-of select="Estatisticas/Vitorias" /></td>
          <td><xsl:value-of select="Estatisticas/Empates" /></td>
          <td><xsl:value-of select="Estatisticas/Derrotas" /></td>
          <td><xsl:value-of select="Estatisticas/GolosMarcados" /></td>
          <td><xsl:value-of select="Estatisticas/GolosSofridos" /></td>
          <td><xsl:value-of select="Estatisticas/MediaMarcados" /></td>
          <td><xsl:value-of select="Estatisticas/MediaSofridos" /></td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

</xsl:stylesheet>
