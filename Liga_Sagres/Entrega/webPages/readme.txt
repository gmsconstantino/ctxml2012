
* Pasta css - contem todos os css utilizados pelas diferentes paginas
* Pasta images - contem unicamente o LOGO da liga usado na pagina
 index.html
* Pasta js - contem os ficheiros javascript utilizados
  * jquery.js - framework de javascript
  * script.js - todo o codigo criado para fazer a integracao das
                diversas funcionalidades

* liga.dtd - ficheiro com a gramatica para o data.xml
* data.xml - ficheiro de dados 'brutos' xml 
* data2.xml - ficherio criado pelo processamento xslt de data.xml com o
              o ficheiro transformation.xsl
* transformation.xsl
* index.html - pagina inicial
* classificacao.xsl - ficheiro xslt para criar a pagina das
       classificacoes
* clubes.xsl e clube.xsl - ficheiros para criar a pagina de informacao
        dos clubes
* estadios.xsl - ficherio xslt para criar a pagina com as informacaoes
        dos estadios
* gameweeks.xsl e gameweek.xsl - ficheiros xslt para criar a pagina com
        a informacao de jogos das diversas jornadas
* players.xsl - ficheiro xslt para criar a pagina com as informacoes de
        todos os jogadores
* searchplayer.xsl e player.xsl - ficheiros para criar as paginas para a
        pesquisa de um jogador. Esta pesquisa tem de usar o nome
        completo do jogador.
