<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="UTF-8"/>
  <xsl:template match="/">
    <html>
      <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/clubes.css" media="all"/>
      </head>
      <xsl:apply-templates />
      <body>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="LigaSagres">
    <div class="leftSide">
      <ul>
      <xsl:for-each select="Clubes/Clube">
        <li>
          <a href="#" class="linkClube">
            <xsl:attribute name="rel">
              <xsl:value-of select="@id" />
            </xsl:attribute>
            <xsl:value-of select="Nome" />
          </a>
        </li>
      </xsl:for-each>
      </ul>
    </div>
    <div id="rightSide">
    </div>
  </xsl:template>

</xsl:stylesheet>
