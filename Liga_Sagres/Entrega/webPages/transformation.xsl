<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="Estadios">
    <Estadios>
      <xsl:for-each select="Estadio">
        <xsl:variable name="est_id" select='@id'/>
        <Estadio>
          <xsl:apply-templates select="@* | node()"/>
          <Jogos>
            <xsl:for-each select="//Jogo">
              <xsl:if test="Local/@ref = $est_id">
                <nEspectadores>
                  <xsl:value-of select="NumEspectadores" />
                </nEspectadores>
              </xsl:if>
            </xsl:for-each>
          </Jogos>
        </Estadio>
      </xsl:for-each>
    </Estadios>
  </xsl:template>

  <xsl:template match="Pessoas">
    <Pessoas>
      <xsl:for-each select="Pessoa">
        <Pessoa id='{@id}' tipo='{@tipo}'>
          <xsl:variable name="pid" select='@id'/>
          <xsl:variable name="titular" select="count(/LigaSagres//Jogo/PlatelCasa//Jogador[@jogadorRef = $pid]) + count(/LigaSagres//Jogo/PlatelVisitante//Jogador[@jogadorRef = $pid]) "/>
          <xsl:apply-templates select="@* | node()"/>
          <Clube>
            <xsl:value-of select="/LigaSagres/Clubes/Clube[Equipa/*/@jogadorRef = $pid]/Nome" />
          </Clube>
          <Golos>
            <xsl:value-of select="count(/LigaSagres//Jogo/GolosCasa//Jogador[@jogadorRef = $pid]) + count(/LigaSagres//Jogo/GolosVisitante//Jogador[@jogadorRef = $pid]) " />
          </Golos>
          <Titular>
            <xsl:value-of select='$titular' />
          </Titular>
          <Jogos>
            <xsl:value-of select="count(/LigaSagres//Jogo//Substituicao/Substituto[@jogadorRef = $pid])+$titular" />
          </Jogos>
        </Pessoa>
      </xsl:for-each>
    </Pessoas>
  </xsl:template>

  <xsl:template match="Jornada">
    <Jornada id="{@id}">
      <xsl:for-each select="Jogo">
        <Jogo id="{@id}">
          <xsl:copy-of select="Data" />
          <xsl:copy-of select="Local" />
          <xsl:copy-of select="NumEspectadores" />
          <Arbitro>
            <xsl:attribute name="ref">
              <xsl:value-of select="Arbitro/@ref" />
            </xsl:attribute>
            <xsl:value-of select="id(Arbitro/@ref)/Nome" />
          </Arbitro>
          <ClubeCasa>
            <xsl:attribute name="ref">
              <xsl:value-of select="ClubeCasa/@ref" />
            </xsl:attribute>
            <xsl:value-of select="id(ClubeCasa/@ref)/Nome" />
          </ClubeCasa>
          <ClubeVisitante>
            <xsl:attribute name="ref">
              <xsl:value-of select="ClubeVisitante/@ref" />
            </xsl:attribute>
            <xsl:value-of select="id(ClubeVisitante/@ref)/Nome" />
          </ClubeVisitante>
          <xsl:copy-of select="GolosCasa" />
          <xsl:copy-of select="GolosVisitante" />
          <xsl:copy-of select="PlatelCasa" />
          <xsl:copy-of select="PlatelVisitante" />
          <xsl:copy-of select="SubstituicoesCasa" />
          <xsl:copy-of select="SubstituicoesVisitante" />
          <xsl:copy-of select="Faltas" />
        </Jogo>
      </xsl:for-each>
      <Estatisticas>
        <NJogos>
          <xsl:value-of select="count(Jogo)" />
        </NJogos>
        <Vitorias>
          <xsl:value-of select="count(Jogo[count(GolosCasa/Golo) != count(GolosVisitante/Golo)])" />
        </Vitorias>
        <Empates>
          <xsl:value-of select="count(Jogo[count(GolosCasa/Golo) = count(GolosVisitante/Golo)])" />
        </Empates>
      </Estatisticas>
    </Jornada>
  </xsl:template>

  <xsl:template match="Clubes">
    <Clubes>
      <xsl:for-each select="Clube">
        <xsl:variable name="cid" select="@id"/>
        <Clube id="{@id}">
          <xsl:copy-of select="Nome" />
          <xsl:copy-of select="Sigla" />
          <xsl:copy-of select="Website" />
          <xsl:copy-of select="Descricao" />
          <Historico>
            <xsl:for-each select="Historico/Classificacao">
              <Classificacao premio="{@premio}">
                <Designacao><xsl:value-of select="id(@premio)/Designacao" />
                </Designacao>
                <xsl:copy-of select="Ano" />
              </Classificacao>
            </xsl:for-each>
          </Historico>
          <EqTecnica>
            <xsl:for-each select="EqTecnica/Membro">
              <Membro pessoaRef="{@pessoaRef}" cargo="{@cargo}">
                <Nome><xsl:value-of select="id(@pessoaRef)/Nome" />
                </Nome>
              </Membro>
            </xsl:for-each>
          </EqTecnica>
          <Equipa>
            <xsl:for-each select="Equipa/*">

              <xsl:element name="{name()}">
                <xsl:attribute name="jogadorRef">
                  <xsl:value-of select="@jogadorRef" />
                </xsl:attribute>
                <xsl:value-of select="id(@jogadorRef)/Nome" />
              </xsl:element>
            </xsl:for-each>
          </Equipa>
          <Estatisticas>
            <xsl:variable name="jogo" select="/LigaSagres/Epocas/Epoca//Jogo"/>
            <xsl:variable name="njogos" select="count($jogo[ClubeCasa/@ref = $cid or ClubeVisitante = $cid])"/>
            <xsl:variable name="glmarc" select="count($jogo[ClubeCasa/@ref = $cid]/GolosCasa/Golo) + count($jogo[ClubeVisitante/@ref = $cid]/GolosVisitante/Golo)"/>
            <xsl:variable name="glsofr" select="count($jogo[ClubeCasa/@ref = $cid]/GolosVisitante/Golo) + count($jogo[ClubeVisitante/@ref = $cid]/GolosCasa/Golo)"/>
            <xsl:variable name="vitorias" select="count($jogo[ClubeCasa/@ref = $cid or ClubeVisitante = $cid][count(GolosCasa/Golo) > count(GolosVisitante/Golo)])"/>
            <xsl:variable name="empates" select="count($jogo[ClubeCasa/@ref = $cid or ClubeVisitante = $cid][count(GolosCasa/Golo) = count(GolosVisitante/Golo)])"/>
            <Pontos>
              <xsl:value-of select="number($vitorias) * 3 + number($empates)" />
            </Pontos>
            <NJogos>
              <xsl:value-of select="number($njogos)" />
            </NJogos>
            <Vitorias>
              <xsl:value-of select="$vitorias" />
            </Vitorias>
            <Empates>
              <xsl:value-of select="$empates" />
            </Empates>
            <Derrotas>
              <xsl:value-of select="$njogos - ($vitorias+$empates)" />
            </Derrotas>
            <GolosMarcados>
              <xsl:value-of select="$glmarc" />
            </GolosMarcados>
            <GolosSofridos>
              <xsl:value-of select="$glsofr" />
            </GolosSofridos>
            <MediaMarcados>
              <xsl:value-of select="$glmarc div $njogos" />
            </MediaMarcados>
            <MediaSofridos>
              <xsl:value-of select="$glsofr div $njogos" />
            </MediaSofridos>
          </Estatisticas>
        </Clube>
      </xsl:for-each>

    </Clubes>
  </xsl:template>



  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>



</xsl:stylesheet>
