Ficheiros presentes na Pasta sao:

* ER.png - Imagem Descritiva do Modelo ER usado para a construcao do DTD
 e Schema
* liga.dtd - Ficheiro com o DTD criado a partir do Modelo ER
* liga.xsd - Ficheiro com o Schema criado a partir do Modelo ER
* liga_dtd.xml - Exemplo Ilustrativo usado como gramatica o DTD
* liga_xsd.xml - Exemplo Ilustrativo usando como gramatica o Schema
